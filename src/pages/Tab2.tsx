import { IonBackButton, IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForwardCircleOutline } from 'ionicons/icons';
import ExploreContainer from '../components/ExploreContainer';
import './Tab2.css';

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton>Cancel</IonBackButton>
          </IonButtons>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonItem fill="solid">
          <IonLabel position="floating">Email</IonLabel>
          <IonInput placeholder="Username@gmail.com"></IonInput>
        </IonItem>
        <IonItem fill="solid">
          <IonLabel position="floating">Password</IonLabel>
          <IonInput type="password" placeholder="Password"></IonInput>
        </IonItem>
        <IonButton shape="round" color="success" expand="block">Login<IonIcon slot="end" icon={arrowForwardCircleOutline}></IonIcon></IonButton>
      </IonContent>
    </IonPage>
  );
};

export default Tab2;
