import {
    IonButton,
    IonContent,
    IonHeader,
    IonInput,
    IonItem,
    IonLabel,
    IonPage,
    IonTitle,
    IonToolbar
} from '@ionic/react';
import {useEffect, useRef, useState} from "react";

const Home: React.FC = () => {
    const [data, setData] = useState('');
    const url = "http://localhost/api/tambahmahasiswa2.php";
    const nim = useRef<HTMLIonInputElement>(null);
    const nama = useRef<HTMLIonInputElement>(null);
    const email = useRef<HTMLIonInputElement>(null);
    const phone = useRef<HTMLIonInputElement>(null);

    const insertHandler = () => {
        const inNim = nim.current?.value;
        const inNama = nama.current?.value;
        const inEmail = email.current?.value;
        const inPhone = phone.current?.value;

        fetch(url, {
            method: "post",
            headers: {"Content-type": "application/json"},
            body:JSON.stringify({
                nim: inNim,
                nama: inNama,
                email: inEmail,
                nomorHp: inPhone
            })
        }).then(response => response.text()).then((data)=>{
            setData(data);
            console.log(data);
        });
    };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonItem>
            <IonLabel position="floating">NIM</IonLabel>
            <IonInput ref={nim}></IonInput>
        </IonItem>
          <IonItem>
              <IonLabel position="floating">Nama</IonLabel>
              <IonInput ref={nama}></IonInput>
          </IonItem>
          <IonItem>
              <IonLabel position="floating">Email</IonLabel>
              <IonInput ref={email}></IonInput>
          </IonItem>
          <IonItem>
              <IonLabel position="floating">Phone</IonLabel>
              <IonInput ref={phone}></IonInput>
          </IonItem>
          <IonButton onClick={insertHandler}>Simpan</IonButton>
      </IonContent>
    </IonPage>
  );
};

export default Home;