import { IonContent,IonRouterOutlet, IonButton,IonHeader, IonPage, IonTitle, IonToolbar,IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonButtons, IonBackButton, IonList, IonItem, IonSelect, IonSelectOption } from '@ionic/react';
import { useEffect ,useState} from 'react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab3.css';
import { Redirect,Route } from 'react-router-dom';
import Tab2 from './Tab2';

type assuranceExpire = {
  marque:String
  matricule:String
  description:String
  dateExpiration:String
  kilometrageInitiale:number
};
const Tab3: React.FC = () => {
const url="http://localhost:8081/assuranceExpirer/1";

  // sendRequest();
const [assuranceExpire,setAssuranceExpirer]=useState<Array<assuranceExpire>>([]);
const sendRequest=()=>{};
const fillStorage=(jsons: string)=>{
  localStorage.setItem("assuranceExpirer",jsons);
};
useEffect(()=>{
  fetch(url, {
    method: "get",
    headers: {"Content-type": "application/json"},
  }).then(response => response.json()).then((data)=>{
  console.log(data);
    fillStorage(data[0]);
    setAssuranceExpirer(data);
  }).catch(err=>{
  console.log(err);
  });
},[]);

// const test = assuranceExpire.map((data)=>{ 
//       return <h2>{data.marque}</h2>});
  return (
    <IonPage>
      <IonContent fullscreen>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton>Cancel</IonBackButton>
          </IonButtons>
          <IonTitle>Expiration Assusrance</IonTitle>
        </IonToolbar>
      </IonHeader>  
      <IonList>
      <IonItem>
        <IonSelect placeholder="Select month">
            <IonSelectOption value="1">1 Month outdated</IonSelectOption>
            <IonSelectOption value="3">3 Months outdated</IonSelectOption>
          </IonSelect>
        </IonItem>
      </IonList>
      { assuranceExpire.map((data)=>{ 
      
        return <div>
          <IonCard> 
          <IonCardHeader>
            <IonCardTitle>{data.marque}</IonCardTitle>
            <IonCardSubtitle>{data.matricule}</IonCardSubtitle>
          </IonCardHeader>
        
          <IonCardContent>
           {data.kilometrageInitiale}km , {data.dateExpiration} , {data.description} 
           </IonCardContent>
          </IonCard>
        </div>})
      
      }
      
   
   
        {/* <ExploreContainer name="Tab 1 page" /> */}
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
