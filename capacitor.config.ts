import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Floud',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
