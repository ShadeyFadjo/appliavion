import { IonContent,IonRouterOutlet, IonButton,IonHeader,IonItem, IonPage, IonTitle, IonToolbar,IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonButtons, IonMenuButton } from '@ionic/react';
import { useEffect ,useState} from 'react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import { Redirect,Route } from 'react-router-dom';
import Tab2 from './Tab2';
import SlideBar from './SlideBar';
import Login from './Login';


var marques=[];
var contents=[];

type avion={
  idAvion:number
  type:String
  photo:String
  numeroserie:String
};
const Tab1: React.FC = () => {
const url="https://appliavion-production.up.railway.app/avion/";

  // sendRequest();
const [avion,setAvion]=useState<Array<avion>>([]);

const sendRequest=()=>{
  
};
const fillStorage=(jsons: string)=>{
  localStorage.setItem("avion",jsons);
};
useEffect(()=>{
  fetch(url, {
    method: "get",
    headers: {"Content-type": "application/json"},
   
}).then(response => response.json()).then((data)=>{
  console.log(data);
    fillStorage(data[0]);
    setAvion(data);
 }).catch(err=>{
  console.log(err);
});
  },[]);

  const test=
    avion.map((data)=>{ 
      return <h2>{data.type}</h2>});
  return (
    <IonPage id="main-content">
      <SlideBar />
      <IonContent fullscreen>
        {/* <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Liste</IonTitle>
          </IonToolbar>
        </IonHeader>   */}
      
      { avion.map((data)=>{ 
      
        return <div>
          <IonCard>
            <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
            <IonCardHeader>
              <IonCardTitle>{data.numeroserie}</IonCardTitle>
              {/* <IonCardSubtitle>{data.matricule}</IonCardSubtitle> */}
            </IonCardHeader>
            <IonCardContent>
            {/* {data.kilometrageinitiale}km */}
              <IonRouterOutlet>
              <Route exact path="/login/:id">
                <Login />
                </Route>
              </IonRouterOutlet>
              <IonItem routerLink={`/login/${data.idAvion}`} detail={false}>Voir details</IonItem>
            </IonCardContent>
          </IonCard>
        </div>})
      
      }
      
   
   
        {/* <ExploreContainer name="Tab 1 page" /> */}
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
