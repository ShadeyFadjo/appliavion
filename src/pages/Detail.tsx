import React from 'react';
import { IonBackButton, IonButtons,IonProgressBar, IonHeader, IonPage, IonToolbar, IonTitle, IonContent,IonMenuButton,IonSegmentButton, IonLabel,IonSegment, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonFab, IonFabButton, IonFabList, IonIcon, IonButton, IonRouterOutlet} from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import { addSharp, camera, chevronForwardCircle, colorPalette, globe, power } from 'ionicons/icons';
import { Route } from 'react-router';
import Login from './Login';
import Tab1 from './Tab1';
import { usePhotoGallery , UserPhoto } from '../hooks/usePhotoGallery';
const Detail: React.FC = () => {
    const {takePhoto} = usePhotoGallery();
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Details Of Plane</IonTitle>
                    <IonProgressBar type="indeterminate"></IonProgressBar>
                </IonToolbar>
            </IonHeader>
            <IonContent>
            <IonFab slot="fixed" vertical="top" horizontal="start">
                <IonFabButton>
                    <IonIcon icon={chevronForwardCircle}></IonIcon>
                </IonFabButton>
                <IonFabList side="end">
                    <IonFabButton href='/tab1'>
                    <Route exact path="/tab1">
                        <Tab1 />
                    </Route>        
                    <IonIcon icon={power}></IonIcon>
                    </IonFabButton>
                    <IonFabButton>
                    <IonIcon icon={camera}></IonIcon>
                    </IonFabButton>
                    <IonFabButton>
                    <IonIcon icon={globe}></IonIcon>
                    </IonFabButton>
                </IonFabList>
            </IonFab>
            <IonCard>
            <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
            <IonCardHeader>
                <IonCardTitle>Boing 741</IonCardTitle>
                <IonCardSubtitle>Serie dgeiqfgdyqe</IonCardSubtitle>
                </IonCardHeader>
                <IonCardContent>12345 km
                    
                    <IonButton expand='block'  onClick={()=> takePhoto()}>Change picture</IonButton>
                </IonCardContent>
            </IonCard>
                <center>
                    <strong><h1>Kilometrage</h1></strong>
                </center>
                <IonCard>
                    <IonCardContent>Kilometrage Debut - Kilometrage Fin</IonCardContent>
                    <IonCardContent>Date Debut - Date Fin</IonCardContent>
                </IonCard>
                <center>
                    <strong><h1>Assurance</h1></strong>
                </center>
                <IonCard>
                    <IonCardContent>Kilometrage Debut - Kilometrage Fin</IonCardContent>
                    <IonCardContent>Date Debut - Date Fin</IonCardContent>
                </IonCard>
                <center>
                    <strong><h1>Entretien</h1></strong>
                </center>
                <IonCard>
                    <IonCardContent>Kilometrage Debut - Kilometrage Fin</IonCardContent>
                    <IonCardContent>Date Debut - Date Fin</IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    );
  };
export default Detail;