import React, { useRef, useState } from 'react';
import { IonBackButton, IonButtons,IonProgressBar, IonHeader, IonPage, IonToolbar, IonTitle, IonContent,IonMenuButton,IonSegmentButton, IonLabel,IonSegment, IonButton, IonIcon, IonInput, IonItem} from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import { arrowForwardCircleOutline } from 'ionicons/icons';
import { useParams } from 'react-router';

const Login: React.FC = () => {
        const params=useParams<{id:string}>();
        const nom = useRef<HTMLIonInputElement>(null);
      const password=useRef<HTMLIonInputElement> (null);
      const [answer,setAnswer]=useState(false);
      const authent=()=>
          {
              // alert("nom:"+nom.current?.value+" password"+password.current?.value );
              console.log(JSON.stringify({
                nom: nom.current?.value,
                pwd: password.current?.value
            }));
              fetch("https://appliavion-production.up.railway.app/employes/"+nom.current?.value+"/"+password.current?.value, {
                method: "post",
                headers: {"Content-type": "application/json"},
                // body:JSON.stringify({
                //     nom: nom.current?.value,
                //     pwd: password.current?.value
                // })
            }).then(response => response.text()).then((data)=>{
              console.log(data)
              if(data=="true"){
                setAnswer(true);window.location.replace(`/detail/${params.id}`);
            }
                else{setAnswer(false);}
              //redirect();
            }).catch(error=>{console.log(error)});
            
          }
        const redirect=()=>{if(answer==true){
          
          // window.location.replace(`/tab3/${params.id}`);
        }
        else{
          window.location.replace(`/login/${params.id}`);
        }};
      
    return (
        <IonPage>
          <IonHeader>
            <IonToolbar>
              <IonButtons slot="start">
                <IonBackButton>Cancel</IonBackButton>
              </IonButtons>
              <IonTitle>Login</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent fullscreen>
            <IonItem fill="solid">
              <IonLabel position="floating">Email</IonLabel>
              <IonInput placeholder="Username@gmail.com" ref={nom} value="user"></IonInput>
            </IonItem>
            <IonItem fill="solid">
              <IonLabel position="floating" >Password</IonLabel>
              <IonInput type="password" value="password" placeholder="Password" ref={password}></IonInput>
            </IonItem>
            <IonItem routerLink={`/detail/${params.id}`} detail={false}>Voir details</IonItem>

            {/* <IonButton shape="round" color="success" expand="block" href='/detail'>Login<IonIcon slot="end" icon={arrowForwardCircleOutline}></IonIcon></IonButton> */}
          </IonContent>
        </IonPage>
    );
  };
export default Login;  
